﻿Param(
    [Hashtable]$communicatieObject
)
#ZORG ALTIJD DAT WebView2Loader.dll in dezelfde map staat!
[void][reflection.assembly]::LoadFile("C:\temp\microsoft.web.webview2.1.0.902-prerelease\lib\net45\Microsoft.Web.WebView2.WinForms.dll")
[void][reflection.assembly]::LoadFile("C:\temp\microsoft.web.webview2.1.0.902-prerelease\lib\net45\Microsoft.Web.WebView2.Core.dll")
[void][reflection.assembly]::Load('System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a')
[void][reflection.assembly]::Load('System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
#endregion Import Assemblies
$communicatieObject = @{}
#----------------------------------------------
#region Generated Form Objects
#----------------------------------------------
$communicatieObject.Form = [System.Windows.Forms.Form]::New()
$communicatieObject.buttonRefresh = New-Object 'System.Windows.Forms.Button'
$communicatieObject.buttonGo = New-Object 'System.Windows.Forms.Button'
$communicatieObject.textbox = New-Object 'System.Windows.Forms.TextBox'
$communicatieObject.WebView2 = [Microsoft.Web.WebView2.WinForms.WebView2]::New()
$communicatieObject.WebView2.CreationProperties = New-Object 'Microsoft.Web.WebView2.WinForms.CoreWebView2CreationProperties'
$communicatieObject.WebView2.CreationProperties.UserDataFolder = "C:\temp";

$communicatieObject.coreWebView2Initialized = {
    # CookieManager only available after the CoreWebView2 property has been initialized. 
    $communicatieObject.cookieManager = $communicatieObject.WebView2.CoreWebView2.CookieManager;
}



$InitialFormWindowState = New-Object 'System.Windows.Forms.FormWindowState'

$communicatieObject.Form_Load={
    #TODO: Initialize Form Controls here
    $communicatieObject.WebView2.Source = ([uri]::new($communicatieObject.textbox.Text))
    $communicatieObject.WebView2.Visible = $true
}

$communicatieObject.buttonGo_Click={
    #TODO: Place custom script here
    $communicatieObject.WebView2.Source = [System.Uri] $communicatieObject.textbox.Text;
}

$communicatieObject.webview_SourceChanged={
    $communicatieObject.Form.Text = $communicatieObject.WebView2.Source.AbsoluteUri;
}

# --End User Generated Script--
#----------------------------------------------
#region Generated Events
#----------------------------------------------

$Form_StateCorrection_Load=
{
    #Correct the initial state of the form to prevent the .Net maximized form issue
    $communicatieObject.Form.WindowState = $InitialFormWindowState
}

$Form_Cleanup_FormClosed=
{
    #Remove all event handlers from the controls
    try
    {
        $communicatieObject.cookieManager.GetCookiesAsync()
        $communicatieObject.buttonGo.remove_Click($communicatieObject.buttonGo_Click)
        $communicatieObject.WebView2.remove_SourceChanged($communicatieObject.webview_SourceChanged)
        $communicatieObject.Form.remove_Load($communicatieObject.Form_Load)
        $communicatieObject.Form.remove_Load($Form_StateCorrection_Load)
        $communicatieObject.Form.remove_FormClosed($Form_Cleanup_FormClosed)
        
        $communicatieObject.WebView2 = $Null
    }
    catch { Out-Null <# Prevent PSScriptAnalyzer warning #> }
}
#endregion Generated Events

#----------------------------------------------
#region Generated Form Code
#----------------------------------------------
$communicatieObject.Form.SuspendLayout()
#
# form1
#
$communicatieObject.Form.Controls.Add($communicatieObject.buttonRefresh)
$communicatieObject.Form.Controls.Add($communicatieObject.buttonGo)
$communicatieObject.Form.Controls.Add($communicatieObject.textbox)
$communicatieObject.Form.Controls.Add($communicatieObject.WebView2)
$communicatieObject.Form.AutoScaleDimensions = New-Object System.Drawing.SizeF(6, 13)
$communicatieObject.Form.AutoScaleMode = 'Font'
$communicatieObject.Form.ClientSize = New-Object System.Drawing.Size(619, 413)
$communicatieObject.Form.Name = 'form1'
$communicatieObject.Form.Text = 'Form'
$communicatieObject.Form.add_Load($communicatieObject.Form_Load)
#
# buttonRefresh
#
$communicatieObject.buttonRefresh.Location = New-Object System.Drawing.Point(13, 13)
$communicatieObject.buttonRefresh.Name = 'buttonRefresh'
$communicatieObject.buttonRefresh.Size = New-Object System.Drawing.Size(75, 23)
$communicatieObject.buttonRefresh.TabIndex = 3
$communicatieObject.buttonRefresh.Text = 'Refresh'
$communicatieObject.buttonRefresh.UseVisualStyleBackColor = $True
#
# buttonGo
#
$communicatieObject.buttonGo.Location = New-Object System.Drawing.Point(538, 9)
$communicatieObject.buttonGo.Name = 'buttonGo'
$communicatieObject.buttonGo.Size = New-Object System.Drawing.Size(75, 23)
$communicatieObject.buttonGo.TabIndex = 2
$communicatieObject.buttonGo.Text = 'Go'
$communicatieObject.buttonGo.UseVisualStyleBackColor = $True
$communicatieObject.buttonGo.add_Click($communicatieObject.buttonGo_Click)
#
# textbox1
#
$communicatieObject.textbox.Location = New-Object System.Drawing.Point(96, 13)
$communicatieObject.textbox.Name = 'textbox1'
$communicatieObject.textbox.Size = New-Object System.Drawing.Size(435, 20)
$communicatieObject.textbox.TabIndex = 1
$communicatieObject.textbox.Text = 'https://www.google.nl'
#
# webview
#
$communicatieObject.WebView2.Location = New-Object System.Drawing.Point(0, 49)
$communicatieObject.WebView2.Name = 'webview'
$communicatieObject.WebView2.Size = New-Object System.Drawing.Size(619, 364)
$communicatieObject.WebView2.TabIndex = 0
$communicatieObject.WebView2.ZoomFactor = 1
$communicatieObject.WebView2.add_SourceChanged($communicatieObject.webview_SourceChanged)

$communicatieObject.Form.ResumeLayout()
#endregion Generated Form Code

#----------------------------------------------

#Save the initial state of the form
$InitialFormWindowState = $communicatieObject.Form.WindowState
#Init the OnLoad event to correct the initial state of the form
$communicatieObject.Form.add_Load($Form_StateCorrection_Load)
#Clean up the control events
$communicatieObject.Form.add_FormClosed($Form_Cleanup_FormClosed)
$communicatieObject.WebView2.add_CoreWebView2InitializationCompleted($communicatieObject.coreWebView2Initialized);
#Show the Form
$communicatieObject.Form.ShowDialog()